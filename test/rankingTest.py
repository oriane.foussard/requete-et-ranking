import unittest
import json
from service.ranking import Ranking


class RankingTest(unittest.TestCase):
    def setUp(self):
        self.ranking = Ranking()

    def test_SimpleRankingFunction(self):
        requetteTT = ["le", "sport"]
        filtedIndex = {
            "le": {
                "0": {"positions": [0], "count": 1},
                "1": {"positions": [0], "count": 1},
                "2": {"positions": [0], "count": 1}
            },
            "sport": {
                "0": {"positions": [1], "count": 1},
                "1": {"positions": [1], "count": 1}
            }}
        expected_output = {
            "0": 2,
            "1": 2,
            "2": 1
        }
        self.assertEqual(self.ranking.SimpleRankingFunction(
            requetteTT, filtedIndex), expected_output)

    def test_StopWordRankingFunction(self):
        requetteTT = ["le", "sport"]
        filtedIndex = {
            "le": {
                "0": {"positions": [0], "count": 1},
                "1": {"positions": [0], "count": 1},
                "2": {"positions": [0], "count": 1}
            },
            "sport": {
                "0": {"positions": [1], "count": 1},
                "1": {"positions": [1], "count": 1}
            }}
        expected_output = {'0': 1.1, '1': 1.1, '2': 0.1}
        self.assertEqual(self.ranking.StopWordRankingFunction(
            requetteTT, filtedIndex), expected_output)

    def test_RankedDoncuments(self):
        rankByDoc = {'0': 1.1, '1': 1.1, '2': 0.1}
        documentFilePath = "test/documents_test.json"
        expected_output = {'ranking': [{'url': 'je suis un url', 'id': 0, 'title': "Le sport c'est bon pour la santé"}, {'url': 'je suis un url', 'id': 1, 'title': 'Le sport pour les nuls'}, {
            'url': 'je suis un url', 'id': 2, 'title': 'Le son du loup'}], 'number_of_documents': 3, 'number_of_filter_survivors': 3}
        self.assertEqual(self.ranking.RankedDoncuments(
            rankByDoc, documentFilePath), expected_output)

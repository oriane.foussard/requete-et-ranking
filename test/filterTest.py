import json
import unittest
from typing import List
from service.filter import Filter


class FilterTest(unittest.TestCase):
    def setUp(self):
        self.filter = Filter('test/index_test.json')

    def test_filterAND(self):
        requestTT = ["le", "sport"]
        filtered_index = self.filter.filterAND(requestTT)
        self.assertEqual(filtered_index, {'le': {'0': {'positions': [0], 'count': 1}, '1': {'positions': [
                         0], 'count': 1}}, 'sport': {'0': {'positions': [1], 'count': 1}, '1': {'positions': [1], 'count': 1}}})

        requestTT = ["poule"]
        filtered_index = self.filter.filterAND(requestTT)
        self.assertEqual(filtered_index, {})

    def test_filterOR(self):
        requestTT = ["le", "sport"]
        filtered_index = self.filter.filterOR(requestTT)
        self.assertEqual(filtered_index, {'le': {'0': {'positions': [0], 'count': 1}, '1': {'positions': [0], 'count': 1}, '2': {
                         'positions': [0], 'count': 1}}, 'sport': {'0': {'positions': [1], 'count': 1}, '1': {'positions': [1], 'count': 1}}})

        requestTT = ["poule"]
        filtered_index = self.filter.filterOR(requestTT)
        self.assertEqual(filtered_index, {})

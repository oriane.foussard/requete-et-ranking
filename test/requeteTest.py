import unittest
from service.requete import Request


class RequestTest(unittest.TestCase):

    def test_getSimplyTokenizedRequest(self):
        request_empty = Request('')
        self.assertTrue(request_empty.getSimplyTokenizedRequest()==[])
        request_wikipedia = Request('Wikipédia')
        self.assertTrue(request_wikipedia.getSimplyTokenizedRequest()==['Wikipédia'])
        request_complex = Request("$OL; '' j\'aime le pain")
        self.assertTrue(request_complex.getSimplyTokenizedRequest()==['$OL;', "''", "j'aime", 'le', 'pain'])

    def test_SimpleTransformation(self):
        request_empty = Request('')
        request_empty.SimpleTransformation()
        self.assertTrue(request_empty.simplyTransformedRequest==[])
        request_wikipedia = Request('Wikipédia')
        request_wikipedia.SimpleTransformation()
        self.assertTrue(request_wikipedia.simplyTransformedRequest==['wikipédia'])
        request_complex = Request("$OL; '' j\'aime le pain")
        request_complex.SimpleTransformation()
        self.assertTrue(request_complex.simplyTransformedRequest==['$ol;', "''", "j'aime", 'le', 'pain'])
import argparse
from service.requete import Request
from service.filter import Filter
from service.ranking import Ranking


if __name__ == '__main__':
   parser = argparse.ArgumentParser()
   parser.add_argument('query', default='', type=str, help='User query')
   parser.add_argument('--type_of_join', choices=['and', 'or'], default='and', help='Choosing if the query includes every token (and) or at least one token (or)')
   parser.add_argument("--documents_path", default='documents.json', type=str, help="the PATH of the file containing documents informations")
   parser.add_argument("--index_path", default='index.json', type=str, help="the PATH of the file containing the index")
   parser.add_argument("--results_path", default='results.json', type=str, help="the PATH of the file containing the results")
   parser.add_argument('--without_stop_word', choices=['true', 'false'], default='false', help='Using true gives a smaller weight to stop words in the ranking function')
   args = parser.parse_args()
   request = Request(args.query)
   request.SimpleTransformation()
   requestTT = request.simplyTransformedRequest
   filtre = Filter(args.index_path)
   type_of_join = args.type_of_join
   if type_of_join=='and' :
      filted = filtre.filterAND(requestTT)
   else : 
      filted = filtre.filterOR(requestTT)
   without_stop_word = args.without_stop_word
   rank = Ranking()
   if without_stop_word =='true' :
      ranked = rank.StopWordRankingFunction(requestTT, filted)
   else :
      ranked = rank.SimpleRankingFunction(requestTT, filted)
   rankedDoc = rank.RankedDoncuments(ranked, args.documents_path)
   rank.WriteResults(args.results_path, rankedDoc)


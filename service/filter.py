import json
from typing import List

class Filter():

    def __init__(self, indexToFilterPath : str) -> None:
        # ouvrir un fichier json
        with open(indexToFilterPath) as f:
            # return l'index au format json
            self.indexToFilter = json.load(f)


    def filterAND(self, requestTT : List[str]) -> json:
        """
        On cherche à filter les documents ont tous les tokens, on va donc créer un index qui ne contient que les informations des documents qui ont tous les tokens.
        Récupérer cet index nous permettra de calculer différentes méthodes de ranking
        """
        #reauestTT correspond à la requete tokenisée et transformée
        # on va crééer un index qui correspond à l'index  des documents qui ont survécu au filtre
        filtedIndex = dict()
        try :
            #on crée la liste des document qui ont le premier token de la requete
            if requestTT[0] in self.indexToFilter.keys():
                listDoc = self.indexToFilter[requestTT[0]].keys()

            #pour chaque token
            for token in requestTT :
                #si le token est dans la liste des token de l'index
                if token in self.indexToFilter.keys() :
                    #on prend l'intersection des document qui ont les tokens précédent et ceux qui ont ce token
                    listDoc = list(set(listDoc) & set(self.indexToFilter[token].keys()))
                    # on trie la liste par ordre croissant (mieux pour la visualisation)
                    listDoc = sorted(listDoc, key=int)
            
            #on crée l'index filtré
            for token in requestTT :
                if token in self.indexToFilter.keys() :
                    filtedIndex[token]=dict()
                    for idDoc in listDoc :
                        filtedIndex[token][idDoc] = self.indexToFilter[token][idDoc]
        except : 
                pass
        return filtedIndex

    def filterOR(self, requestTT : List[str]) -> json:
        filtedIndex = {}
        for token in requestTT :
            if token in self.indexToFilter.keys() :
                #on récupère l'ensemble de l'index pour ce token (par ce que tous les documents qui ont ce token ont au moins un des tokkens de la requête)
                filtedIndex[token] = self.indexToFilter[token]
        return filtedIndex

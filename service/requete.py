from typing import List


class Request():

    def __init__(self, request: str) -> None:
        self.request = request
        self.simplyTransformedRequest = []

    def getSimplyTokenizedRequest(self) -> List[str]:
        # on decoupe la requête en mot
        return self.request.split()

    def SimpleTransformation(self) -> None:
        #pour chaque token on va le mettre en minuscule
        self.simplyTransformedRequest = [token.lower() for token in self.getSimplyTokenizedRequest()]
        return 
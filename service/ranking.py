import json
from typing import List

class Ranking():

    def __init__(self,) -> None:
        pass

    def SimpleRankingFunction(self, requetteTT : List[str], filtedIndex : json) -> json :
        #reauestTT correspond à la requete tokenisée et transformée
        # on va calculer un score pour chacun des documentsqui on résisté au filtre
        rankByDoc = {}
        # pour caque token
        for token in requetteTT :
            if token in filtedIndex.keys():
                # pour chaque document où apparait le token
                for idDoc in filtedIndex[token] :
                    # si on a déjà associé un score au document, on lui ajoute la valeur du count
                    if idDoc in rankByDoc.keys() :
                        rankByDoc[idDoc] += filtedIndex[token][idDoc]['count']
                    # sinon on associe au document la valeur du count où apparait le token (c'est initialisation)
                    else :
                        rankByDoc[idDoc] = filtedIndex[token][idDoc]['count']
        return rankByDoc

    def StopWordRankingFunction(self, requetteTT : List[str], filtedIndex : json) -> json :
        rankByDoc = {}
        facteur = 1

        # Récupérer la liste des stop word
        stopWords = [] # future liste des stopWords
        with open("service/stop_word.txt", "r", encoding="utf-8") as f:
            for ligne in f:
                ligne = ligne.rstrip() # supprime la fin de ligne
                stopWords.append(ligne) # ajoute la ligne à la liste

        #calcul ranking en prenant compte des stop words
        for token in requetteTT :
            if token in filtedIndex.keys():
                if token in stopWords :
                    facteur = 0.1 # on accorde 10 fois moins d'importance au stop word de la requête qu'aux autres tokens
                else :
                    facteur = 1
                for idDoc in filtedIndex[token] :
                    if idDoc in rankByDoc.keys() :
                        rankByDoc[idDoc] += filtedIndex[token][idDoc]['count']*facteur
                    else :
                        rankByDoc[idDoc] = filtedIndex[token][idDoc]['count']*facteur
        return rankByDoc

    def RankedDoncuments(self, rankByDoc : dict, documentFilePath : str) -> dict :
        # ouvrir un fichier json
        with open(documentFilePath) as f:
            # return une liste de dictionnaire contenant les infos sur les documents
            document = json.load(f)

        #on va renvoyer les documents qui ont survecu au filtre ordonnée par ordre dècroissant de score rank
        rankedDoc = []
        #on récupère les documents correspondant ordonnée
        for idDoc, rank in sorted(rankByDoc.items(), key=lambda x: x[1], reverse=True):
            rankedDoc.append(document[int(idDoc)])
        # on organise le résultat pour qu'il contienne les information s sur le nombre de documents de départ et le nombre de documents qui ont survécu au filtre
        result = dict()
        result['ranking'] = rankedDoc
        result['number_of_documents'] = len(document)
        result['number_of_filter_survivors'] = len(rankByDoc.keys())
        return result 

    def WriteResults(self, resultPath : str, result : dict):
        resultsStr = json.dumps(result, ensure_ascii=False).encode('utf8').decode()
        with open(resultPath, "w", encoding="utf-8") as file:
            file.write(resultsStr)
        return 
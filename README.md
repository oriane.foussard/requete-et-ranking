# Index

## Description

Ce projet est basé sur un index fourni au format json, dans le fichier `index.json`, et un ensemble de documents fourni au format json, dans le fichier `documents.json`. Ce projet contient l'implémentation d'un système qui : 
-lit une requête de l’utilisateur
-La tokenise et la transforme pour filtrer les documents 
-ordonne les documments à l'aide d'une fonction de ranking linéaire pour ordonner les documents qui auront survécu au filtre. 
-Renvoie une liste de documents au format json

La fonnction de ranking que j'ai tout d'abord implémenté associe à un document le score calculé comme suit : la somme des 'count' ( count = nombre de fois où un token apparait dans le titre du document) des token de la rêquete.


## Installation du projet

La première étape, installation du projet :

```
git clone https://gitlab.com/oriane.foussard/requete-et-ranking
cd requete-et-ranking
pip install -r requirements.txt
```
verfiez que vous posedez une version de `python3`.


## Lancer la construction de l'index
```
python3 main.py query --type_of_join --index_path  --documents_path --without_stop_word --results_path

```

L'argument `query` correspond à la requête de l'utilisateur.

L'argument `--type_of_join` correspond au type de filte que l'on veut appliquer. Il prend deux valeurs : 'and' ou 'or'. Il permet de transformer la requête pour qu’elle prenne en compte les documents qui ont tous les tokens de la requête ('and'),ou ceux qui en ont au moins un ('or'). Il vaut par défaut `and`.

L'argument `--documents_path` correspond au chemin d'accès relatif (PATH) du fichier json contenant les informations sur les documents. Il vaut par défaut `documents.json`. Ce doit être une liste de dictionnaire de la forme :
```
[{"url": url, "id": docId, "title": titre}]
```

L'argument `--index_path` correspond au chemin d'accès relatif (PATH) du fichier json contenant l'index positionnel. Il vaut par défaut `index.json`. Ce doit être un dictionnaire de la forme :
```
Token: {docId: {position: [position indices], count: int}
```

L'argument `--without_stop_word` correspond à un booléen qui vaut true si on veut associer un poid plus important aux tokens qui ont du sens par rapport aux stop words au moment du ranking, sinon on donne la même important aux deux types de tokens.

L'argument `--results_path` correspond au chemin d'accès relatif (PATH) du fichier json contenant les résutats de notre ranking. Il vaut par défaut `results.json`. C'est une liste de la forme : 
```
{'ranking' : [{"url": url, "id": docId, "title": titre}], 'number_of_documents' : numberdoc, 'number_of_filter_survivors' : numbersur}
```
La valeur de la clé ranking correspond qux les documents qui on suvis qu filtre et order par ordre décroissant du score de ranking. La valeur de la clé number_of_documents correspond au le nombre de documents dans l’index, et la valeur de la clé number_of_filter_survivors correspond au nombre de documents qui ont survécu au filtre.

Le code relatif au traitements de la requête se trouve dans le fichier `service/requete.py`. Le code relatif au filtrage de l'index se trouve dans le fichier `service/filter.py`. Le code relatif à la fonction de ranking se trouve dans le fichier `service/ranking.py`. Ce code est documenté et commenté, chaque choix d'implémentation est expliqué.

## Lancer les tests

```
python3 -m unittest test/test.py
```


## Authors

Oriane Foussard.
